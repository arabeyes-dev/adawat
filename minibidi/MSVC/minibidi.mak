# Microsoft Developer Studio Generated NMAKE File, Based on minibidi.dsp
!IF "$(CFG)" == ""
CFG=minibidi - Win32 Release
!MESSAGE No configuration specified. Defaulting to minibidi - Win32 Release.
!ENDIF 

!IF "$(CFG)" != "minibidi - Win32 Release"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "minibidi.mak" CFG="minibidi - Win32 Release"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "minibidi - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

OUTDIR=.\bin
INTDIR=.\temp
# Begin Custom Macros
OutDir=.\bin
# End Custom Macros

ALL : "$(OUTDIR)\minibidi.dll"


CLEAN :
	-@erase "$(INTDIR)\minibidi.obj"
	-@erase "$(INTDIR)\minishape.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(OUTDIR)\minibidi.dll"
	-@erase "$(OUTDIR)\minibidi.exp"
	-@erase "$(OUTDIR)\minibidi.lib"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "MINIBIDI_EXPORTS" /Fp"$(INTDIR)\minibidi.pch" /YX /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
RSC=rc.exe
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\minibidi.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /incremental:no /pdb:"$(OUTDIR)\minibidi.pdb" /machine:I386 /def:".\minibidi.def" /out:"$(OUTDIR)\minibidi.dll" /implib:"$(OUTDIR)\minibidi.lib" 
DEF_FILE= \
	".\minibidi.def"
LINK32_OBJS= \
	"$(INTDIR)\minibidi.obj" \
	"$(INTDIR)\minishape.obj"

"$(OUTDIR)\minibidi.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("minibidi.dep")
!INCLUDE "minibidi.dep"
!ELSE 
!MESSAGE Warning: cannot find "minibidi.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "minibidi - Win32 Release"
SOURCE=.\..\minibidi.c

"$(INTDIR)\minibidi.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\..\minishape.c

"$(INTDIR)\minishape.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)



!ENDIF 

