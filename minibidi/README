
========
miniBidi
========

minibidi is available as a part of the Adawat project avaiable from

http://www.arabeyes.org/project.php?proj=Adawat


Download
========
minibidi's source is available from arabeyes' CVS.

by ViewCVS:
http://cvs.arabeyes.org/viewcvs/projects/adawat/minibidi/

by pserver:
CVSROOT=:pserver:anoncvs@cvs.arabeyes.org:/home/arabeyes/cvs
when asked for a password use 'anoncvs'.


What is minibidi?
=================
minibidi is an implementation of Unicode's Bidirectional Algorithm (UAX#9).
basicly minibidi just reorders text strings containing Left aligned characters
like Latin, and Right aligned characters like Arabic or Hebrew.
and also does shaping, which is needed in the Arabic Language.

The Bidirectional Algorithm is applied by calling the function doBidi() and
passing the input text buffer, and the count of characters in the buffer. the
input buffer is 2-byte wide characters. minibidi currently supports the
Unicode range of U+0000 to U+FFFF.

minibidi's Data types can be changed/customized to your needs by changing
BLOCKTYPE, CHARTYPE, GETCHAR defines to suit your structures.

Shaping is applied during the doBidi() call to every run separately, as per
the Algorithm's recomendation. To control wether the buffer is shaped or not,
use True (1) or False (0) for the third argument of doBidi. If you're sure 
that your buffer doesn't contain any arabic characters, you can pass False
(or 0) to speed up the doBidi() call.

Transparent chars are reordered according to the algorithm depending on the
fourth argument of doBidi(). If you're sure your buffer doesn't contain any
transparent chars, use False (or 0) for this argument to speed up the 
doBidi() call.

Explicit marks are automatically removed from the source line, and any
removed marks result in the line being shortened. be carefull when using
this with your own data structures with GETCHAR as this might result
in the charachter data changing place but not other structure members.

If you want to keep track of explicit marks' positions (e.g. in a text 
editor) use other characters as place holders for them.
e.g. U+FFFC; Object Replacement Character

Why Another Bidi?
=================
Fribidi and ICU are very respectable and massive libraries. The overhead of
using such libraries might be too much for some projects. minibidi is a
direct implementation of UAX #9, without the need to use the libraries'
Datatypes, or Engine Initialization. Just include the source files and call the
main function.

Shaping
=======
If you're interested in Arabic shaping only, use doShape().
call doShape() with the source line, the destination line, where to start
the shaping at, and the count of characters to shape.
Before calling doShape() be sure to copy the source line into the destination
line, as some loop iterations are skipped and the characters are not copied.
This is going into the TODO section

Testing
=======
To run the tests, 

goto minibidi.c at line 38
comment "#define GetType(x) getType(x)" and
uncomment "#define GetType(x) getCAPRtl(x)"
this makes miniBidi use the CAPRtl charset used for testing

then run,
$ gcc testrun.c -o mini
$ ./mini

This will run the tests directly from minitests.txt. or use
$ ./mini testfile.txt

to run tests from another file.

miniBidi currently passes 34 of 35 tests. miniBidi passes all
implicit tests.

the failed test is ( ; <-has type RLE. : <- has type LRE)
- INPUT         THE ;best :ONE and
- OUTPUT        best ENO and EHT
- miniBidi      ENO and best EHT
unfortunately the ';' is resolved at level 3 and replaced by a BN in the
reference C implementation, minibidi removes the ';' after resolving its
level. later on 'best' takes over and increases the level to 4. miniBidi
has no trace of the 3 so it misses a flip.
I currently dont have any solution for this failed test.

Licence
=======
minibidi is available under the MIT licence, explained in the file LICENCE.
basicly it means, you can do whatever you want with minibidi's source, and
i wont be liable or be held responsible for any damages or losses.

Links
=====
Arabic Shaping
http://www.unicode.org/Public/UNIDATA/ArabicShaping.txt
Bidi Mirroring
http://www.unicode.org/Public/UNIDATA/BidiMirroring.txt
Unicode Data
http://www.unicode.org/Public/UNIDATA/UnicodeData.txt


TODO
====
- Memory Alignement in the shaping table.
- doBidi() optimization
- doMirror() optimization. hash table ?
  + Fixed, using a binary search tree
- shaping table complete ?
- Optimize doShape() by removing the copy src->dest first requirement


Contact or Contribute
=====================
minibidi is an active development project, any comments, suggestions, feedback,
bugs, etc... is welcomed.

contact:  developer -at- arabeyes.org

