// testrun.cpp : Defines the entry point for the console application.
//

#include "string.h"
#include "malloc.h"
#include "stdlib.h"
#include "stdio.h"

#ifdef _WIN32
#include "conio.h"
#endif

#include "minibidi.c"

typedef struct{
	char* test;
	char* result;
}testcase;

testcase* testvector[100];
int testtop = 0;

int loadCases(char* filename)
{
	char buffer[256];
	char* separate;

	FILE* fp = fopen(filename, "rt");
	if(!fp)
		return 0;
	while(!feof(fp))
	{
		if(fgets(buffer, 255, fp))
		{
			testcase* t = (testcase*)malloc(sizeof(testcase));
			separate = strchr(buffer, '#');
			if(!separate)
				continue;
			*separate = 0;
			t->test = strdup(buffer);
			t->result = strdup(++separate);
			testvector[testtop++] = t;
		}
	}

	return testtop;
}

int runTest(int index)
{
	printf("\nRunning Test %d...", index);

	int count = strlen(testvector[index]->test);
	wchar_t* line = (wchar_t*)malloc(sizeof(wchar_t) * count + 1);
	wchar_t* resultline = (wchar_t*)malloc(sizeof(wchar_t) *  strlen(testvector[index]->result) + 1);
	mbstowcs(line, testvector[index]->test, count);
	mbstowcs(resultline, testvector[index]->result, strlen(testvector[index]->result));
	count = doBidi(line, count, 0, 0);
	//doShape(line, resultline, 0, count);
	if(wcsncmp(line, resultline, count) == 0)
	{
		printf("   - TEST %d SUCCEEDED", index);
		return 1;
	}else
	{
	        char* bidires = malloc(sizeof(char) * (count+1));
		bidires[count] = 0;
		wcstombs(bidires, line, count);
		printf("   ! TEST %d FAILED", index);
		printf("\n! ORIG %d: %s ", index, testvector[index]->test);
		printf("\n! BIDI %d: %s ", index, bidires);
		printf("\n! XPEC %d: %s ", index, testvector[index]->result);
		return 0;
	}
}

int main(int argc, char* argv[])
{
	printf("miniBidi Test!\n");
	int nFails = 0;	

	int numTests;
	if(argc < 2)
		numTests = loadCases("minitests.txt");
	else
		numTests = loadCases(argv[1]);

	int i;
	for(i=0; i<numTests; i++)
	{
		if(!runTest(i))
			nFails++;
	}

	if(nFails > 0)
		printf("\nSUMMARY: completed %d tests, with %d failures!\n", numTests, nFails);
	else
		printf("\nSUMMARY: completed %d tests successfully!\n", numTests);

#ifdef _WIN32
	printf("\nPress Any Key to exit...");
	getch();
#endif
	return 0;
}

