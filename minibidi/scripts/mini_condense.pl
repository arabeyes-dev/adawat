#!/usr/bin/perl
# -*-Perl-*-

#---
# $Id$
#
# ------------
# Description:
# ------------
#  A condenser of various table tags specified in input file.
#  In other words, itstead of having the same pattern repeat
#  multiple times, the script counts those characters/tags
#  and reports them back in a condensed manner (instead of
#  100 'L' characters, you get (100,  L) as a result).
#
#  A user is able to specify which address region to do this
#  on given the input file adheres to a simplistic format
#  (noted below).
#
# -----------------
# Revision Details:    (Updated by Revision Control System)
# -----------------
#  $Date$
#  $Author$
#  $Revision$
#  $Source$
#
# (www.arabeyes.org - under MIT license - by Nadim Shaikli)
#---

require "newgetopt.pl";

##
# Specify global variable values and init some Variables
$this_script    = $0;
$this_script    =~ s|^.*/([^/]*)|$1|;

# Process the command line
&get_args();

if ( $opt_help ) { &help; }

# User specified options
if ($opt_all)
{
    $default_start	= "0";
    $default_end	= "FFFFFFFF";
}
$addr_start	= $opt_start	|| $default_start;
$addr_end	= $opt_end	|| $default_end;
$filename	= $opt_file	|| "utable.c";

# Make sure we have something to work with
if ( (!defined $addr_start) || (!defined $addr_end) )
{
    &usage(1);
}

open (INFILE, $filename) or die "Can't open input $filename\n";

print "/* Using $this_script */\n";
print "/* - Hex Range  : $addr_start - $addr_end */\n";
print "/* - Input File : $filename */\n";

# Here is a sample input file:
# /* 0090 */ BN, BN, BN, BN, BN, BN, BN, BN, BN, BN, BN, BN, BN, BN, BN, BN,
# /* 00A0 */ CS, ON, ET, ET, ET, ET, ON, ON, ON, ON, L, ON, ON, ON, ON, ON,

while (<INFILE>)
{
    # Remove any carriage-returns from input file
    chop;

    # See if we have a line we should care about
    # - NOTE: this is not exact (ie. 0xC3 ignores entire 0xC0 line)
    if (/\/\*\s*(\S+)\s*\*\//)
    {
	$do_the_work	= ( (hex($1) >= hex($addr_start)) &&
			    (hex($1) <= hex($addr_end)) );
    }

    # Now we have something to hack on
    if ($do_the_work)
    {
	# Get rid of the comment section on each line
	$_	=~ s/\/\*.*\*\///;

	# Split-up the lines on ","
	@terms	= split(/,/);

	# Deal with each entry post-split
	foreach $entry (@terms)
	{
	    # Get rid of spaces
	    $entry =~ s/\s+//g;

	    # Do the silly counting of consecutive strings/data
	    if ( ($entry eq $saved) && ($count < 255) )
	    {
		$count++;
	    }
	    else
	    {
		# If I'm out of the loop and I have something saved report it
		if (defined $saved)
		{
		    $num_done++;
		    &print_em($num_done, $count, $saved);
		}
		# Else I'm starting with somethin new baby
		$saved	= $entry;
		$count	= 1;
	    }
	}
    }
}

# Make sure to print the very last entry (if any)
if ($count)
{
    &print_em($num_done, $count, $saved);
}
print "\n";

# Normal exit
exit(0);

       ###        ###
######## Procedures ########
       ###        ###

##
# Report out what we've gathered
sub print_em
{
    my($num_printed,
       $count,
       $saved)		= @_;

    # I like the (..) notation, but go nuts :-)
#    printf ("%02x, %2s ",$count, $saved);
    printf ("(%02x, %2s) ",$count, $saved);

    # Print only a specific number of things per line
    if (!($num_printed % 8))
    {
	print "\n";
    }
}

##
# Print short usage info
sub usage
{
    my ($die_after) = @_;

    ##
    # Find how many spaces the scripts name takes up (nicety)
    $this_spaces    = $this_script;
    $this_spaces    =~ s/\S/ /g;

    print qq
|Usage: $this_script <-all> <-start hex_value>
       $this_spaces        <-end hex_value>
       $this_spaces        [-file filename]
       $this_spaces        [-help]
|;

    if ( $die_after ) { exit(5); }
}

##
# Print one-liner help
sub help
{
    &usage(0);

    print qq|
-> generates a condensed marking table based on input file

  Options:

    <-all>                  : Specify to process entire file
    <-start hex_address>    : Specify starting hex address (0)
    <-end hex_address>      : Specify ending hex address (FFFF_FFFF)
    [-file filename]        : Specify input filename (utable.c)

|;
    exit(10);
}

##
# Get the command line arguments
sub get_args
{
  &NGetOpt (
            "help",			# Print a nice help screen
            "all",			# Specify processing of entire file
            "start=s",			# Specify a starting address
            "end=s",			# Specify an ending address
            "file=s",			# Specify filename to process
	   ) || ( $? = 257, die "Invalid argument\n" );
}
