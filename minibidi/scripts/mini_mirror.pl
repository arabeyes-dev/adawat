#!/usr/bin/perl
$filename = "BidiMirroring.txt";
# Sample input:
#
#    0028; 0029 # LEFT PARENTHESIS
#    0029; 0028 # Comment
#    003C; 003E # Comment
# Sample output:
#
#    {0x0028, 0x0029},
#    {0x0029, 0x0029},
open (INFILE, "$filename") or die "Can't open $filename \n";
while (<INFILE>)
{
# Get rid of the trailing \n in input file
chomp;
# Turn input into an array
@fields             = split(/;\s+/, $_);
# Make sure to grab the one number field (no spaces and crap)
$fields[1]          =~ s/(\S+)\s*#(.*)/$1/;
# My modification to print the new format
print "  {0x$fields[0], 0x$fields[1]},\n";
# Make an associative array out of it
$assoc{$fields[0]}  = $fields[1];

}

