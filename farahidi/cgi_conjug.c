#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mysql.h>

#include "vrb_rule.h"

FILE        *fdlog;


char *get_next_token(char *query, char *token);
int  token_to_tv(char *token, char *tag, char *val);

char *get_next_token(char *query, char *token) {

   while (*query && (*query != '&')) {
      *token++ = *query++;
   }
   *token = '\0';
   if (*query == '&') query++;

   return query;
}

int  token_to_tv(char *token, char *tag, char *val) {
   unsigned char table[16] = {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
      0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f};

   while (*token && (*token != '=')) {
      *tag++ = *token++;
   }
   *tag = '\0';
   if (*token == '=') {
      token++;
      while (*token) {
         if (*token == '+') {
            *val++ = ' ';
            token++;
         }
         else if (*token == '%') {
            token++;
            *val = 0;
            if ((*token >= '0') && (*token < '9')) *val = table[*token - '0'] << 4;
            else *val = table[*token - 'A' + 10] << 4;
            token++;
            if ((*token >= '0') && (*token < '9')) *val |= table[*token - '0'];
            else *val |= table[*token - 'A' + 10];
            val++;
            token++;
         }
         else {
            *val++ = *token++;
         }
      }
      *val = '\0';
   }
   else {
      *val = '\0';
   }
   return 0;
}

int get_arabic_person(char *epers, char *apers) {

   if (strcmp(epers, "sing_speak") == 0) {
      strcpy(apers, "متكلّم مفرد: ");
   }
   else if (strcmp(epers, "plur_speak") == 0) {
      strcpy(apers, "متكلّم جمع: ");
   }
   else if (strcmp(epers, "sing_male_adrs") == 0) {
      strcpy(apers, "مخاطب مفرد مذكّر: ");
   }
   else if (strcmp(epers, "sing_fem_adrs") == 0) {
      strcpy(apers, "مخاطب مفرد مؤنّث: ");
   }
   else if (strcmp(epers, "two_adrs") == 0) {
      strcpy(apers, "مخاطب مثنّى: ");
   }
   else if (strcmp(epers, "plur_male_adrs") == 0) {
      strcpy(apers, "مخاطب جمع مذكّر: ");
   }
   else if (strcmp(epers, "plur_fem_adrs") == 0) {
      strcpy(apers, "مخاطب جمع مذكّر: ");
   }
   else if (strcmp(epers, "sing_male_abs") == 0) {
      strcpy(apers, "غائب مفرد مذكّر: ");
   }
   else if (strcmp(epers, "sing_fem_abs") == 0) {
      strcpy(apers, "غائب مفرد مؤنّث: ");
   }
   else if (strcmp(epers, "two_male_abs") == 0) {
      strcpy(apers, "غائب مثنّى مذكّر: ");
   }
   else if (strcmp(epers, "two_fem_abs") == 0) {
      strcpy(apers, "غائب مثنّى مؤنّث: ");
   }
   else if (strcmp(epers, "plur_male_abs") == 0) {
      strcpy(apers, "غائب جمع مذكّر: ");
   }
   else if (strcmp(epers, "plur_fem_abs") == 0) {
      strcpy(apers, "غائب جمع مؤنّث: ");
   }
   else {
      apers[0] = '\0';
   }

   return 0;
}

int main (int argc, char *argv[], char *envp[]) {
   int         count, count1, nRows, nbFields, countRule, nRules;
   char        query[512];
   MYSQL       *myData;
   MYSQL_RES	*res, *resRule;
   MYSQL_FIELD	*fd;
   MYSQL_ROW   row, rowRule;
   char        hostName[] = "localhost";
   char        userName[] = "root";
   char        password[] = "1234";
   char        dbName[]   = "arab_verbs_rules";
   char        verb[50], result[250], token[80], *ptr;
   char        tag[30], val[80];
   int         ruleId;
   int         cjgId[9] = {11, 12, 13, 14, 21, 22, 23, 24, 30};
   char        isCjgId[9] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
   char        cjgName[9][30] = {"الماضي المعلوم", "المضارع المعلوم المرفوع", "المضارع المعلوم المنصوب", "المضارع المعلوم المجزوم", "الماضي المجهول", "المضارع المجهول المرفوع", "المضارع المجهول المنصوب", "المضارع المجهول المجزوم", "الأمر"};
   char        rule[80], apers[80], *qq, *pRule, *pTmp, *pRes;
   char        persons[256], sqlQuery[512];
   char        aszFlds[30][50];

   printf ("HTTP/1.1 1 1 200\n");
   printf ("Content-Type: text/plain; charset=UTF-8\n\n");

   myData = mysql_init((MYSQL*)0);
   if (myData == NULL) {
      printf ("SORRY: Unable to start a database session.\n");
      exit(0);
   }

   if (mysql_real_connect(myData, hostName, userName, password, dbName, 
      MYSQL_PORT, NULL, 0) == NULL) {
      printf ("SORRY: Error connecting to db %s on host %s as %s with passwd(%s).\n",
                       dbName, hostName, userName, password);
      mysql_close(myData);
      exit(0);
   }
   fdlog = fopen("c:\\winnt\\temp\\essai.log", "w");

   qq = getenv("QUERY_STRING");
   if (qq) {
      strcpy(query, qq);
   }
   else {
      query[0] = '\0';
   } 
//   strcpy(query, "vrb2cjg=%C3%D8%D1&R11=T&R12=T&R13=T&R14=T&R21=T&R22=T&R23=T&R24=T&R30=T&PMS=T&PMP=T&AMS=T&AFS=T&AMD=T&AMP=T&AFP=T&BMS=T&BFS=T&BMD=T&BFD=T&BMP=T&BFP=T&UOB_CJG=%C5%DA%D1%C8");

   fprintf (fdlog, "%s\n", query);

   ptr = query;
   persons[0] = '\0';
   while (*ptr) {
      ptr = get_next_token(ptr, token);
      token_to_tv(token, tag, val);
      if (strcmp(tag, "vrb2cjg") == 0) {
         strcpy(verb, val);
      }
      else if (tag[0] == 'R') {
         ruleId = atoi(&(tag[1]));
         switch (ruleId) {
         case 11:
            isCjgId[0] = 0x01;
            break;
         case 12:
            isCjgId[1] = 0x01;
            break;
         case 13:
            isCjgId[2] = 0x01;
            break;
         case 14:
            isCjgId[3] = 0x01;
            break;
         case 21:
            isCjgId[4] = 0x01;
            break;
         case 22:
            isCjgId[5] = 0x01;
            break;
         case 23:
            isCjgId[6] = 0x01;
            break;
         case 24:
            isCjgId[7] = 0x01;
            break;
         case 30:
            isCjgId[8] = 0x01;
            break;
         default:
            break;
         }
      }
      else {
         if (strcmp(tag, "PMS") == 0) {
            if (persons[0]) strcat(persons, ", sing_speak");
            else strcpy(persons, "sing_speak");
         }
         else if (strcmp(tag, "PMP") == 0) {
            if (persons[0]) strcat(persons, ", plur_speak");
            else strcpy(persons, "plur_speak");
         }
         else if (strcmp(tag, "AMS") == 0) {
            if (persons[0]) strcat(persons, ", sing_male_adrs");
            else strcpy(persons, "sing_male_adrs");
         }
         else if (strcmp(tag, "AFS") == 0) {
            if (persons[0]) strcat(persons, ", sing_fem_adrs");
            else strcpy(persons, "sing_fem_adrs");
         }
         else if (strcmp(tag, "AMD") == 0) {
            if (persons[0]) strcat(persons, ", two_adrs");
            else strcpy(persons, "two_adrs");
         }
         else if (strcmp(tag, "AMP") == 0) {
            if (persons[0]) strcat(persons, ", plur_male_adrs");
            else strcpy(persons, "plur_male_adrs");
         }
         else if (strcmp(tag, "AFP") == 0) {
            if (persons[0]) strcat(persons, ", plur_fem_adrs");
            else strcpy(persons, "plur_fem_adrs");
         }
         else if (strcmp(tag, "BMS") == 0) {
            if (persons[0]) strcat(persons, ", sing_male_abs");
            else strcpy(persons, "sing_male_abs");
         }
         else if (strcmp(tag, "BFS") == 0) {
            if (persons[0]) strcat(persons, ", sing_fem_abs");
            else strcpy(persons, "sing_fem_abs");
         }
         else if (strcmp(tag, "BMD") == 0) {
            if (persons[0]) strcat(persons, ", two_male_abs");
            else strcpy(persons, "two_male_abs");
         }
         else if (strcmp(tag, "BFD") == 0) {
            if (persons[0]) strcat(persons, ", two_fem_abs");
            else strcpy(persons, "two_fem_abs");
         }
         else if (strcmp(tag, "BMP") == 0) {
            if (persons[0]) strcat(persons, ", plur_male_abs");
            else strcpy(persons, "plur_male_abs");
         }
         else if (strcmp(tag, "BFP") == 0) {
            if (persons[0]) strcat(persons, ", plur_fem_abs");
            else strcpy(persons, "plur_fem_abs");
         }
         else {
         }
      }
   }
   fprintf (fdlog, "verb: %s\n persons: %s \n", verb, persons);
   for (count = 0;count < 9;count++) {
      if (isCjgId[count]) 
         fprintf(fdlog, "%s\n", cjgName[count]);
   }

   sprintf(sqlQuery, "SELECT ruleId FROM verbs WHERE verb=\'%s\'", verb);
   fprintf (fdlog, "\n\nquery %s\n", sqlQuery);
   if (mysql_query(myData, sqlQuery) == 0) {
      resRule = mysql_store_result (myData);
      nRules = (int)mysql_num_rows(resRule);
      for (countRule = 0; countRule < nRules; countRule++) {
         rowRule = mysql_fetch_row(resRule);
         ruleId = atoi(rowRule[0]);
//         ruleId = 37;
         printf ("------------------------------------------------\n");
         printf ("------------------------------------------------\n");
         printf("الفعل: %s\n", verb);
         printf("القاعدة %d\n", ruleId);
         printf ("------------------------------------------------\n");
         printf ("------------------------------------------------\n");
         fprintf (fdlog, "\n\nrule id: %d\n", ruleId);
         if (persons[0]) {
            for (count = 0;count < 9;count++) {
               if (isCjgId[count]) {
                  sprintf (sqlQuery, "SELECT %s FROM verb_rules WHERE rule_id=%d AND conjug_id=%d",
                     persons, ruleId, cjgId[count]);
                  if (mysql_query(myData, sqlQuery) == 0) {
                     res = mysql_store_result (myData);
                     nRows = (int)mysql_num_rows(res);
                     if (nRows != 0) {
                        printf ("\n------------------------------------------------\n");
                        printf ("\n%s:\n", cjgName[count]);
                        fprintf (fdlog, "\n------------------------------------------------\n");
                        fprintf (fdlog, "\n%s:\n", cjgName[count]);
                        for (count1 = 0; fd = mysql_fetch_field(res); count1++)
                           strcpy(aszFlds[count1], fd->name);
                        while (row = mysql_fetch_row(res)) {
                           nbFields = mysql_num_fields(res);
                           for (count1 = 0; count1 < nbFields; count1++) {
                              get_arabic_person(aszFlds[count1], apers);
                              strcpy(rule, row[count1]);
                              if ((strcmp(rule, "-")) && (strcmp(rule, "NULL"))) {
                                 pRule = rule;
                                 pRes = result;
                                 while (pRule) {
                                    pTmp = strchr(pRule, '|');
                                    if (pTmp) {
                                       *pTmp = '\0';
                                    }
                                    vrb_apply_rule(verb, pRule, pRes, 50);
                                    if (pTmp) {
                                       strcat(pRes, ", ");
                                       pRes += strlen(pRes);
                                       *pTmp = '|';
                                       pTmp++;
                                    }
                                    pRule = pTmp;
                                 }
                              }
                              else {
                                 strcpy(result, "-");
                              }
                              printf("%s  %s\n", apers, result);
                              fprintf(fdlog, "%s  %s (%s)\n", apers, result, rule);
                           }
                        }
                     } 
                     mysql_free_result(res);
                  }
               }
            }
         }
      }
      mysql_free_result(resRule);
   }
   else {
      printf ("%s!@#\n", verb);
   }
   mysql_close(myData);
   fprintf (fdlog, "MYSQL closed!!!\n");

   fclose(fdlog);
//   fclose(stdout);

//   return 0;
//   exit(0);
}

