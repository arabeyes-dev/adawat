/****************************************************************************/
/*																	*/
/*							tst_vrb_rule.c								*/
/*							C. Mokbel								*/
/*																	*/
/*	Function:	This module implements necessary function for the application		*/
/*			of verbs grammatical rules.									*/
/*	Date:	August 10, 2003											*/
/*																	*/
/****************************************************************************/
#include <stdio.h>
#include <string.h>

#include "vrb_rule.h"



int main (int argc, char *argv[]) {
   char inStr[100], outStr[100], rule[100];
   FILE *fd;

   /*
   fprintf (stdout, "Input the verb: ");
   scanf ("%s", inStr);
   fprintf (stdout, "Input the rule: ");
   scanf ("%s", rule);
   */
   strcpy (inStr, "ضَرَبَ");
   strcpy (rule,  "L1aL2aL3oStu");
   vrb_apply_rule(inStr, rule, outStr, 100);
   fd = fopen ("toto", "wb");
   fprintf(fd, "%s", outStr);
   fclose(fd);

}
