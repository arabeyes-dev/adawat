/****************************************************************************/
/*																	*/
/*							vrb_rule.h								*/
/*							C. Mokbel								*/
/*																	*/
/*	Function:	This module implements necessary function for the application		*/
/*			of verbs grammatical rules.									*/
/*	Date:	August 10, 2003											*/
/*																	*/
/****************************************************************************/
#ifndef VRB_RULE_H_ 
#define VRB_RULE_H_  v1.0






#ifdef __cplusplus
extern "C" {
#endif

// This function permits to apply a rule to a string and to get
// the transformed verb
int  vrb_apply_rule(char *src, char *rule, char *dst, int len);






#ifdef __cplusplus
}
#endif

#endif


