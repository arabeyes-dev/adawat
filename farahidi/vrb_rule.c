/****************************************************************************/
/*																	*/
/*							vrb_rule.c								*/
/*							C. Mokbel								*/
/*																	*/
/*	Function:	This module implements necessary function for the application		*/
/*			of verbs grammatical rules.									*/
/*	Date:	August 10, 2003											*/
/*																	*/
/****************************************************************************/
#include <stdio.h>
#include <string.h>
#include "arabic_hex_data.h"

const char tahrik[] = "َُِّْ";

// Local functions
int  vrb_rem_tahrik(char *src, char *dst);
int vrb_get_corresp_arabic_char(char eng);


int  vrb_rem_tahrik(char *src, char *dst) {
   
   while (*src) {
      if (strchr(tahrik, *src)) {
         src++;
      }
      else {
         *dst++ = *src++;
      }
   }
   *dst = '\0';
   return 0;
}

int vrb_get_corresp_arabic_char(char eng) {
   switch (eng) {
   case 'a':
      return a_FATHA;
   case 'u':
      return a_DAMMA;
   case 'i':
      return a_KASRA;
   case 'o':
      return a_SUKUN;
   case 'A':
      return a_ALEF;
   case 'U':
      return a_WAW;
   case 'y':
   case 'I':
      return a_YEH;
   case '#':
      return a_HAMZA;
   case '_':
      return a_ALEF_MADDA;
   case 't':
      return a_TEH;
   case 'n':
      return a_NOON;
   case 'm':
      return a_MEEM;
   case '~':
      return a_SHADDA;
   case 'z':
      return a_ZAIN;
   case 'q':
      return a_ALEF_MAKSURA;
   default:
      fprintf (stderr, "UNKNOWN character %c to be translated to Arabic", eng);
      return 0;
   }
}


// This function permits to apply a rule to a string and to get
// the transformed verb
int  vrb_apply_rule(char *src, char *rule, int *dst, int len) {
   char  tmpStr[80];
   char  *pRule, *ptr;
   int   curPos = 0, numLtr;
   char  tmpChar;

   vrb_rem_tahrik(src, tmpStr);

   pRule = rule;
   while (*pRule) {
      if (curPos == len) {
         fprintf (stderr, "Error the transformed verb is longer than %d\n", len);
         return -1;
      }
      if (*pRule == 'P') {
         pRule++;
         while ((*pRule) && (*pRule != 'L')) {
            if (!*pRule) {
               fprintf (stderr, "Error decoding the Prefix: Letter expected.\n");
               return -1;
            }
            if (pRule[1] == '#') {
               switch(*pRule) {
               case 'A':
                  *dst++ = a_ALEF_HAMZA_ABOVE;
                  curPos++;
                  break;
               case 'U':
                  *dst++ = a_WAW_HAMZA;
                  curPos++;
                  break;
               case 'I':
                  *dst++ = a_ALEF_HAMZA_BELOW;
                  curPos++;
                  break;
               default:
                  fprintf (stderr, "Unable to decode the Prefix %c%c\n", pRule[0], pRule[1]);
                  return -1;
                  break;
               }
               pRule += 2;
            }
            else {
               *dst = vrb_get_corresp_arabic_char(*pRule);
               if (*dst == 0) {
                  fprintf (stderr, "Unable to translate %c\n", *pRule);
                  return -1;
               }
               if (((*(dst-1) == a_ALEF) || (*(dst-1) == a_ALEF_HAMZA_ABOVE) || (*(dst-1) == a_ALEF_HAMZA_BELOW)) && (*dst == a_ALEF_MADDA)){
                  *(dst - 1) = *dst;
                  *dst = '\0';
               }
               else {
                  dst++;
                  curPos++;
               }
               pRule++;
            }
         }
      }
      else if (*pRule == 'L') {
         pRule++;
         if (!isdigit(*pRule)) {
            fprintf(stderr, "Number of letter expected in %s !!!!\n", pRule);
            return -1;
         }
         ptr = pRule;
         while (isdigit(*ptr)) ptr++;
         tmpChar = *ptr;
         *ptr = '\0';
         numLtr = atoi(pRule);
         *ptr = tmpChar;
         pRule = ptr;
         switch (tmpChar) {
         case 'x':
            pRule++;
            continue;
         case 'T':
            pRule++;
            break;
         case '=':
            pRule += 2;
            *dst++ = tmpStr[numLtr-1];
            curPos++;
            break;
         default:
            *dst++ = tmpStr[numLtr-1];
            curPos++;
            break;
         }
         while (*pRule && (*pRule != 'L') && (*pRule != 'S')) {
            if (curPos == len) {
               fprintf (stderr, "Error the transformed verb is longer than %d\n", len);
               return -1;
            }
            switch (*pRule) {
            case '(':
               pRule++;
               while (*pRule != ')') {
                  if ((pRule[1] == '#') && ((pRule[0] == 'A') || (pRule[0] == 'U') || (pRule[0] == 'I'))) {
                     switch (pRule[0]) {
                     case 'A':
                        *dst++ = a_ALEF_HAMZA_ABOVE;
                        curPos++;
                        break;
                     case 'U':
                        *dst++ = a_WAW_HAMZA;
                        curPos++;
                        break;
                     case 'I':
                        *dst++ = a_ALEF_HAMZA_BELOW;
                        curPos++;
                        break;
                     }
                     pRule += 2;
                  }
                  else {
                     *dst = vrb_get_corresp_arabic_char(*pRule);
                     if (*dst == 0) {
                        fprintf (stderr, "Unable to translate %c\n", *pRule);
                        return -1;
                     }
                     if (((*(dst-1) == a_ALEF) || (*(dst-1) == a_ALEF_HAMZA_ABOVE) || (*(dst-1) == a_ALEF_HAMZA_BELOW)) && (*dst == a_ALEF_MADDA)){
                        *(dst - 1) = *dst;
                        *dst = '\0';
                     }
                     else {
                        dst++;
                        curPos++;
                     }
                     pRule++;
                  }
                  if (*pRule == '\0') {
                     fprintf (stderr, "Unexpected end of rule expecting )\n");
                     return -1;
                  }
               }
               pRule++;
               break;
            case 'D':
               pRule++;
               *dst = vrb_get_corresp_arabic_char(*pRule);
               if (*dst == 0) {
                  fprintf (stderr, "Unable to translate %c\n", *pRule);
                  return -1;
               }
               if (((*(dst-1) == a_ALEF) || (*(dst-1) == a_ALEF_HAMZA_ABOVE) || (*(dst-1) == a_ALEF_HAMZA_BELOW)) && (*dst == a_ALEF_MADDA)){
                  *(dst - 1) = *dst;
                  *dst = '\0';
               }
               else {
                  dst++;
                  curPos++;
               }
               *dst++ = tmpStr[numLtr-1];
               curPos++;
               pRule++;
               *dst = vrb_get_corresp_arabic_char(*pRule);
               if (*dst == 0) {
                  fprintf (stderr, "Unable to translate %c\n", *pRule);
                  return -1;
               }
               if (((*(dst-1) == a_ALEF) || (*(dst-1) == a_ALEF_HAMZA_ABOVE) || (*(dst-1) == a_ALEF_HAMZA_BELOW)) && (*dst == a_ALEF_MADDA)){
                  *(dst - 1) = *dst;
                  *dst = '\0';
               }
               else {
                  dst++;
                  curPos++;
               }
               pRule++;
               break;
            default:
               if ((pRule[1] == '#') && ((pRule[0] == 'A') || (pRule[0] == 'U') || (pRule[0] == 'I'))) {
                  switch (pRule[0]) {
                  case 'A':
                     *dst++ = a_ALEF_HAMZA_ABOVE;
                     curPos++;
                     break;
                  case 'U':
                     *dst++ = a_WAW_HAMZA;
                     curPos++;
                     break;
                  case 'I':
                     *dst++ = a_ALEF_HAMZA_BELOW;
                     curPos++;
                     break;
                  }
                  pRule += 2;
               }
               else {
                  *dst = vrb_get_corresp_arabic_char(*pRule);
                  if (*dst == 0) {
                     fprintf (stderr, "Unable to translate %c\n", *pRule);
                     return -1;
                  }
                  if (((*(dst-1) == a_ALEF) || (*(dst-1) == a_ALEF_HAMZA_ABOVE) || (*(dst-1) == a_ALEF_HAMZA_BELOW)) && (*dst == a_ALEF_MADDA)){
                     *(dst - 1) = *dst;
                     *dst = '\0';
                  }
                  else {
                     dst++;
                     curPos++;
                  }
                  pRule++;
               }
               break;
            }
         }
      }
      else if (*pRule == 'S') {
         pRule++;
         while (*pRule) {
            if (curPos == len) {
               fprintf (stderr, "Error the transformed verb is longer than %d\n", len);
               return -1;
            }
            if ((pRule[1] == '#') && ((pRule[0] == 'A') || (pRule[0] == 'U') || (pRule[0] == 'I'))) {
               switch (pRule[0]) {
               case 'A':
                  *dst++ = a_ALEF_HAMZA_ABOVE;
                  curPos++;
                  break;
               case 'U':
                  *dst++ = a_WAW_HAMZA;
                  curPos++;
                  break;
               case 'I':
                  *dst++ = a_ALEF_HAMZA_BELOW;
                  curPos++;
                  break;
               }
               pRule += 2;
            }
            else {
               *dst = vrb_get_corresp_arabic_char(*pRule);
               if (*dst == 0) {
                  fprintf (stderr, "Unable to translate %c\n", *pRule);
                  return -1;
               }
               if (((*(dst-1) == a_ALEF) || (*(dst-1) == a_ALEF_HAMZA_ABOVE) || (*(dst-1) == a_ALEF_HAMZA_BELOW)) && (*dst == a_ALEF_MADDA)){
                  *(dst - 1) = *dst;
                  *dst = '\0';
               }
               else {
                  dst++;
                  curPos++;
               }
               pRule++;
            }
         }
      }
      else {
         fprintf (stderr, "Error while applying the rule %s when decoding %s.\n", rule, pRule);
         return -1;
      }
   }
   *dst = '\0';

   return curPos;
}
