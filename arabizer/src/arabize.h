/***************************************************************************
 *   Copyright (C) 2004 by Ahmad Kamal                                     *
 *   eng_ak@link.net                                                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef ARABIZE_H
#define ARABIZE_H

#include <string>
#include <iostream>
#include <fstream>
#include <vector>

//! The Arabize class. Instantiate an instance from this class, and call its transliterate() method to use the Arabize functionality

class arabize {
	std::vector<std::string> arabic;		///<Array for arabic letters
	std::vector<std::string> english;		///<Array for english letters
	std::vector<std::string> comments;	///<Array for comments (ignored)
	
public:
	arabize();				///<Constructor
	~arabize();				///<Destructor
	std::string transliterate(std::string);		///<The core engine call which transliterates the arabic words written by English characters into true arabic characters
private:
	void print_arabic_array();		///<For Debugging. Prints the dictionary array as read from the dictionary file
	void print_english_array();		///<For Debugging. Prints the dictionary array as read from the dictionary file
};
#endif
