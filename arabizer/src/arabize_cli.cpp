/***************************************************************************
 *   Copyright (C) 2004 by Ahmad Kamal                                     *
 *   eng_ak@link.net                                                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>
#include <fstream>
#include <string>
#include "arabize.h"

//!Welcome to Arabize. A program to transliterate arabic text, into true (Arabic characters) Arabic wirting. I hope you will find Arabize useful.
int main()
{
	std::string user_ip="mr7ba bkm m3 arabiz, aD3`t ";	//!<Greeting msg
	std::string text_out="";		//!<Variable where we receive the transliterated text
	arabize engine;
	std::cout << "Welcome to Arabize Console, ^D to exit" << std::endl;	//!<Non-Arabs using Arabize?! No problem
	std::cout << engine.transliterate(user_ip) << 
		"^D" << engine.transliterate(" ll7`rog") << std::endl;
	while (std::getline(std::cin,user_ip)){		//Enter main loop
		text_out = engine.transliterate(user_ip);
		//std::cout << user_ip << std::endl;
		std::cout << text_out << std::endl;
	}
}
