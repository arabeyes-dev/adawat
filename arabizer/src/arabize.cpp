/***************************************************************************
 *   Copyright (C) 2004 by Ahmad Kamal                                     *
 *   eng_ak@link.net                                                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <iostream>
#include "arabize.h"

//!Constructor call, reads the dictionary file & prepares for dynamic storage the std::vector template
arabize::arabize(){
#ifdef _DEBUG
	std::cout << "In arabize constructor" << std::endl;
#endif
	std::ifstream file_ip("hroof_utf8.txt");	//Read dictionary file
	if (not file_ip) std::cout << "Error loading hroof_utf8.txt file" << std::endl;

	std::string line;
	while(std::getline(file_ip, line)){
		arabic.push_back(line);	// Add arabic character
		std::getline(file_ip, line);	//? Needs error checking
		english.push_back(line);	// Add english character
		std::getline(file_ip, line);	//? Needs error checking
		comments.push_back(line);	// Add comment (just read it)
	}
	//print_arabic_array();		//Debugging
	//print_english_array();	//Debugging
	
}

//!The destructor doesn't really do anything cool, coz the arabize object is created on the stack
arabize::~arabize(){
#ifdef _DEBUG
	std::cout << "In arabize destructor" << std::endl;
#endif
}

//!This is the main call. It takes an input string, and for each two (or one) characters in the input finds a match in the transliteration dictionary, and writes back the output string. If not match is found, the character is echoed as is.
std::string  arabize::transliterate(std::string user_ip){
#ifdef _DEBUG
	std::cout << "In arabize Transliterator" << std::endl;
#endif
	std::string CharOrTwo;	//!<Variable holding the one or two characters we are searching for a match for
	std::string text_out;	//!<Holds the Arabic processed text
	bool MatchFound=false;	//!<Temporary internal varaiable
	
	if (user_ip.length() < 1) { std::cout << "Write some text first!" << std::endl; return "";}
	for(unsigned int i=0; i < user_ip.length() ; ++i){
		//Loops over all input characters
		
		if (i < (user_ip.length()-1)){	///<If not at last char
			CharOrTwo = user_ip.at(i);
			CharOrTwo += user_ip.at(i+1);	//Take next 2 chars
			for(unsigned int ctr = 0; ctr < arabic.size(); ctr++)
				if (CharOrTwo == english[ctr] ){	//Are they in our dictionary?
					MatchFound=true;
					text_out += arabic[ctr];		//Put their Arabic equivalent
				}
			if (MatchFound) {MatchFound=false; i++;continue;}//i++ bec we worked on 2 chars
		}
		//Next 2 chars didn't match, or we're at last char.
		//So, let's match this char we are at
		CharOrTwo = user_ip.at(i);
		for(unsigned int ctr = 0; ctr < arabic.size(); ctr++)	//Again is it in dictionary
			if (CharOrTwo == english[ctr] ){
				MatchFound=true;
				text_out += arabic[ctr];		//Put their Arabic equivalent
			}
			if (MatchFound) {MatchFound=false;continue;}
		//We land here, only if we get an un-matched character
		//So, we just append the english one as it is
		text_out += CharOrTwo;

	}//Done iterating over all input chars
#ifdef _DEBUG
	std::cout << "Input:" << user_ip << std::endl << 
		"Output:" << text_out << std::endl;
#endif
	return text_out;
}

//!Prints the arabic array as read from the dictionary
void arabize::print_arabic_array(){
	for(unsigned int i = 0; i < arabic.size(); i++)
	std::cout << i << ": \t" << arabic[i] << std::endl;
}

//!Prints the english array as read from the dictionary
void arabize::print_english_array(){
	//Print array
	for(unsigned int i = 0; i < english.size(); i++)
	std::cout << i << ": \t" << english[i] << std::endl;
}
